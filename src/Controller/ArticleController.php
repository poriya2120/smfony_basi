<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class ArticleController extends AbstractController { 

    /**
     * @Route("/")
     * @Method({"GET})
     */
    
    public function index(){
    //     return new  Response('<html><body>Hello world</body></html>');
        $articles=['title one','title two'];
      //  return $this->render('articles/index.html.twig',array('name'=>'brad'));
        return $this->render('articles/index.html.twig',array('articles'=>$articles));

}
}

